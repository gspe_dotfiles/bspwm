#!/usr/bin/env bash

wid=$1
class=$2
instance=$3
title=$(xtitle $wid)

if [[ "$class" == plasmashell ]]; then
    #  echo "manage=off"
#    echo "layer=above"
    echo "state=floating"
    echo "border=off"
  xdo above -t "$(xdo id -N Bspwm -n root | sort | head -n 1)" $wid
# elif [[ "$class" == lattedock ]]; then
#   echo "state=floating"
#   echo "border=off"
# #   xdo above -t "$(xdo id -N Bspwm -n root | sort | head -n 1)" $wid
#   xdo above -t "$(xdo id -N plasmashell | sort | head -n 1)" $wid
fi
